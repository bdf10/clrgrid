# clrgrid

![solarized palette](./solarized-860x360.png)

Welcome to `clrgrid`!

## Description

`clrgrid` is a go program that generates acceptable lookiing
desktop wallpaper. Have a favorite color palette? Feed it to
`clrgrid` and generate a decent looking `png` image that
fits perfectly on your monitor.

## Basic Usage

`clrgrid` is customizable, but you can 70-IQ your way to a
totally fine grid by compiling the program and issuing
`./clrgrid`.

## Advanced Usage

The features of `clrgrid` are documented with the `-h` flag.

    $ ./clrgrid -h
    Usage of ./clrgrid:
      -density int
        	Density. (default 1)
      -directory string
        	png directory. (default ".")
      -height int
        	Canvas height. (default 1080)
      -palette string
        	Palette name. (default "solarized")
      -stroke int
        	Stroke width.
      -style string
        	Tiling style. (default "square")
      -width int
        	Canvas width. (default 1920)
